package com.mmalaenko.service.impl;

import com.mmalaenko.model.Product;
import com.mmalaenko.repository.ProductRepository;
import com.mmalaenko.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ProductServiceImpl implements ProductService {
    private  final ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        return productRepository.getAll();
    }

    @Override
    public Product getProductByName(String productName) {
        return productRepository.getProductByName(productName);
    }
}
