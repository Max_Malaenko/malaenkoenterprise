package com.mmalaenko.service.impl;

import com.mmalaenko.model.Order;
import com.mmalaenko.repository.OrderRepository;
import com.mmalaenko.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Override
    public void saveOrder(Order order) {
    orderRepository.saveOrder(order);
    }

    @Override
    public List<Order> getListOrderByUser(int userID) {
        return orderRepository.getListOrderByUser(userID);
    }
}