package com.mmalaenko.utill;


        import com.mmalaenko.config.AppConfiguration;
        import org.springframework.web.WebApplicationInitializer;
        import org.springframework.web.context.AbstractContextLoaderInitializer;
        import org.springframework.web.context.ContextLoaderListener;
        import org.springframework.web.context.WebApplicationContext;
        import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
        import org.springframework.web.context.support.XmlWebApplicationContext;

        import javax.servlet.ServletContext;
        import javax.servlet.ServletException;

public class ApplicationInitializer  extends AbstractContextLoaderInitializer {
    @Override
    protected WebApplicationContext createRootApplicationContext() {
        AnnotationConfigWebApplicationContext rootContext
                = new AnnotationConfigWebApplicationContext();
        rootContext.register(AppConfiguration.class);
        return rootContext;
    }
}
